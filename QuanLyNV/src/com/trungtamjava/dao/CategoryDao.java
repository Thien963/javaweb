package com.trungtamjava.dao;

import java.util.List;

import com.trungtamjava.model.Category;

public interface CategoryDao {
	public void add(Category c);

	void edit(Category p);

	void delete(int id);

	List<Category> getAll();
}
