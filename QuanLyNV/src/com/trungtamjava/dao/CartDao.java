package com.trungtamjava.dao;

import java.util.List;

import com.trungtamjava.model.Cart;
import com.trungtamjava.model.CartItem;

public interface CartDao {
	void insert(Cart cart);

	void edit(Cart newCart);

	void delete(int id);

	Cart get(int id);
	
	List<Cart> getAll();

	List<CartItem> search(int id);

}
