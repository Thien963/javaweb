package com.trungtamjava.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.trungtamjava.dao.CartDao;
import com.trungtamjava.dao.JDBCConnection;
import com.trungtamjava.model.Cart;
import com.trungtamjava.model.CartItem;
import com.trungtamjava.model.Product;
import com.trungtamjava.model.User;

public class CartDaoImpl extends JDBCConnection implements CartDao {

	@Override
	public void insert(Cart cart) {

		Connection con = super.getJDBCConnection();

		try {
			String sql = "INSERT INTO Cart(id_user, buydate) VALUES (?,?)";
			PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, cart.getBuyer().getId());
			ps.setDate(2, new Date(cart.getBuyDate().getTime()));
			ps.executeUpdate();
			ResultSet generatedKeys = ps.getGeneratedKeys();
			if (generatedKeys.next()) {
				int id = generatedKeys.getInt(1);
				cart.setId(id);// set vao doi tuong de su dung trong ham main sau nay
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void edit(Cart newCart) {
		String sql = "UPDATE cart SET id_user = ?, buydate = ? WHERE id = ?";
		Connection con = super.getJDBCConnection();

		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, newCart.getBuyer().getId());
			ps.setDate(2, new Date(newCart.getBuyDate().getTime()));
			ps.setInt(3, newCart.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void delete(int id) {
		Connection con = super.getJDBCConnection();

		try {
			String sql = "DELETE FROM cart WHERE id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public Cart get(int id) {
		Connection conn = super.getJDBCConnection();

		try {
			String sql = "SELECT cart.id, cart.buydate , user.name, cart.id_user as id_user "
					+ " FROM cart INNER JOIN user " + " on cart.id_user= user.id WHERE cart.id=?";
			PreparedStatement statement = conn.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id_user"));
				user.setName(rs.getString("name"));

				Cart cart = new Cart();
				cart.setId(rs.getInt("id"));

				cart.setBuyer(user);

				return cart;

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public List<Cart> getAll() {
		List<Cart> list = new ArrayList<Cart>();

		Connection conn = super.getJDBCConnection();

		try {
			String sql = "SELECT cart.id, cart.buydate , user.name, cart.id_user as id_user "
					+ " FROM cart INNER JOIN user " + " on cart.id_user= user.id";
			PreparedStatement statement = conn.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id_user"));
				user.setName(rs.getString("name"));

				Cart cart = new Cart();
				cart.setId(rs.getInt("id"));
				cart.setBuyDate(rs.getDate("buydate"));

				cart.setBuyer(user);

				list.add(cart);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return list;
	}

	@Override
	public List<CartItem> search(int id) {
		List<CartItem> list = new ArrayList<CartItem>();

		Connection conn = super.getJDBCConnection();

		try {
			String sql = "SELECT cart_item.id_product ,product.name, cart_item.buy_quantity, cart_item.sell_price, cart.id "
					+ " FROM cart_item " + " INNER JOIN cart on cart_item.cart_id= cart.id "
					+ " INNER JOIN product on cart_item.id_product= product.id " + " WHERE cart.id=?";

			PreparedStatement statement = conn.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				Product product= new Product();
				product.setId(rs.getInt("id_product"));
				product.setName(rs.getString("name"));
				
				CartItem cartItem= new CartItem();
				cartItem.setBuyQuantity(rs.getInt("buy_quantity"));
				cartItem.setSellPrice(rs.getInt("sell_price"));
				cartItem.setProduct(product);
				
				Cart cart= new Cart();
				cart.setId(rs.getInt("id"));
				
				cartItem.setCart(cart);
				

				list.add(cartItem);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return list;
	}
}
