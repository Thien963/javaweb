package com.trungtamjava.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.trungtamjava.dao.JDBCConnection;
import com.trungtamjava.dao.ProducDao;
import com.trungtamjava.model.Category;
import com.trungtamjava.model.Product;

public class ProductDaoImpl extends JDBCConnection implements ProducDao {

	@Override
	public void add(Product product) {
		Connection conn= super.getJDBCConnection();
		
		try {
			String sql =" INSERT INTO `shop_kienvu`.`product` ( `name`, `price`, `quantity`, `describe`, `image`) VALUES (?,?,?,?,?)";
			PreparedStatement statement= conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, product.getName());
			statement.setInt(2, product.getPrice());
			statement.setInt(3, product.getQuantity());
			statement.setString(4, product.getDescribe());
			statement.setString(5, product.getImage());
			
			statement.executeUpdate();
			
			ResultSet generatedKeys = statement.getGeneratedKeys();
			if (generatedKeys.next()) {
				int id = generatedKeys.getInt(1);
				product.setId(id);// set vao doi tuong de su dung trong ham main sau nay
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void edit(Product product) {
		Connection conn= super.getJDBCConnection();
		
		try {
			String sql="Update product set product.name=?, price=?, quantity=?, describe=?, image=? where id=?";
			PreparedStatement statement= conn.prepareStatement(sql);
			statement.setString(1, product.getName());
			statement.setInt(2, product.getPrice());
			statement.setInt(3, product.getQuantity());
			statement.setString(4, product.getDescribe());
			statement.setString(5, product.getImage());
			statement.setInt(6, product.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
				
	}

	@Override
	public void delete(int id) {
		Connection conn= super.getJDBCConnection();
		
		try {
			String sql="delete from product where id=?";
			PreparedStatement statement= conn.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public List<Product> getAll(String name) {
		Connection conn= super.getJDBCConnection();
		List<Product> list= new ArrayList<Product>();
		try {
			String sql="Select product.name, product.price, product.quantity, product.describe, product.image, category.name as cname, product.id "
					+ " from category inner join "
					+ " product on category.id= product.id_category  where product.name like ?";
			PreparedStatement statement= conn.prepareStatement(sql);
			statement.setString(1, "%" + name + "%");
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				Product product = new Product();
				product.setId(rs.getInt("id"));
				product.setName(rs.getString("name"));
				product.setPrice(rs.getInt("price"));
				product.setQuantity(rs.getInt("quantity"));
				product.setDescribe(rs.getString("describe"));
				product.setImage(rs.getString("image"));
				Category category= new Category();
				category.setName(rs.getString("cname"));
				product.setCategory(category);
				list.add(product);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		return list;
	}

	@Override
	public Product getByProductId(int id) {
		Connection conn= super.getJDBCConnection();
	
		try {
			String sql="select * from product where id=? ";
			PreparedStatement statement= conn.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet rs= statement.executeQuery();
			while(rs.next()) {
				Product product = new Product();
				product.setId(rs.getInt("id"));
				product.setName(rs.getString("name"));
				product.setPrice(rs.getInt("price"));
				product.setQuantity(rs.getInt("quantity"));
				product.setDescribe(rs.getString("describe"));
				product.setImage(rs.getString("image"));
				return product;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public List<Product> getByCategoryId(int id) {
		Connection conn= super.getJDBCConnection();
		List<Product> list= new ArrayList<Product>();
		try {
			String sql="SELECT product.id, product.name, product.price , product.image, category.id as cid " + 
					" FROM category INNER JOIN " + 
					" product on product.id_category= category.id where category.id=?";
			PreparedStatement statement= conn.prepareStatement(sql);
			statement.setInt(1, id);
			ResultSet rs= statement.executeQuery();
			while(rs.next()) {
				Category category= new Category();
				category.setId(rs.getInt("cid"));
				Product product= new Product();
				product.setId(rs.getInt("id"));
				product.setName(rs.getString("name"));
				product.setPrice(rs.getInt("price"));
				product.setImage(rs.getString("image"));
				product.setCategory(category);
				list.add(product);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<Product> searchByName(String name) {
		
		return null;
	}

}
