package com.trungtamjava.controller.clien;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.trungtamjava.model.CartItem;
@WebServlet(urlPatterns="/member/cart/delete")
public class DeleteCart extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pid = req.getParameter("productId");
		HttpSession session = req.getSession();
		Object object = session.getAttribute("cart");
		if (object != null) {
			Map<Integer, CartItem> map = (Map<Integer, CartItem>) object;
			map.remove(Integer.parseInt(pid));

			// cập nhập vào session
			session.setAttribute("cart", map);

		}
		resp.sendRedirect(req.getContextPath() + "/member/cart/list");
	}
}
