package com.trungtamjava.controller.clien;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.trungtamjava.model.User;
import com.trungtamjava.service.UserService;
import com.trungtamjava.service.impl.UserServiceImpl;

@WebServlet(urlPatterns="/add/person")

public class AddUser extends HttpServlet{
	
	private static final String ROLE_MEMBER = "MEMBER";
	UserService userSerivce= new UserServiceImpl();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher dispatcher= req.getRequestDispatcher("/View/client/addUser.jsp");
		dispatcher.forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			User user= new User();
			DiskFileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload servletFileUpload = new ServletFileUpload(factory);
			List<FileItem> items = servletFileUpload.parseRequest(req);
			for(FileItem item: items) {
				if(item.getFieldName().equals("name")) {
					String name=item.getString();
					user.setName(name);
				}else if(item.getFieldName().equals("uname")) {
					String uname= item.getString();
					user.setuName(uname);
				}else if(item.getFieldName().equals("pass")) {
					String pass=item.getString();
					user.setPass(pass);
				}else if(item.getFieldName().equals("file")) {
					String location = "F:\\anh_product\\";
					String fileName = System.currentTimeMillis() + ".jsp";
					File file = new File(location + fileName);
					item.write(file);
					user.setAvatarFileName(fileName);
				}
			}
			String role= ROLE_MEMBER;
			user.setRole(role);
			
			userSerivce.iput(user);
			
			resp.sendRedirect(req.getContextPath()+"/dang-nhap");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
