package com.trungtamjava.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.trungtamjava.model.Cart;
import com.trungtamjava.model.CartItem;
import com.trungtamjava.service.CartService;
import com.trungtamjava.service.impl.CartServiceImpl;

@WebServlet(urlPatterns = "/admin/thongke/chitiet")
public class DetailedStatisticsController extends HttpServlet {// thống kê chi tiết với từng đơn hàng
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CartService cartService = new CartServiceImpl();
		String id = req.getParameter("cid");

		List<CartItem> cart = cartService.search(Integer.parseInt(id));
		req.setAttribute("carts", cart);

		RequestDispatcher dispatcher = req.getRequestDispatcher("/View/admin/cart/chitiet.jsp");
		dispatcher.forward(req, resp);

	}
}
