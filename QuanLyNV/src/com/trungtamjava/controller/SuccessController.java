package com.trungtamjava.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/thanhcong")
public class SuccessController extends HttpServlet {// trả về trang đặt hàng thành công và xâu lại danh sách đặt hàng
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		
		RequestDispatcher dispatcher= req.getRequestDispatcher("/View/admin/cart/success.jsp");
		dispatcher.forward(req, resp);
	}

}
