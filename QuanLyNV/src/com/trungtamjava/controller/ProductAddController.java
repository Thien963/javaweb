package com.trungtamjava.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.trungtamjava.model.Product;
import com.trungtamjava.service.ProductService;
import com.trungtamjava.service.impl.ProductServiceImpl;

@WebServlet(urlPatterns = "/admin/add/product")

public class ProductAddController extends HttpServlet {
	ProductService productService = new ProductServiceImpl();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher dispatcher = req.getRequestDispatcher("/View/admin/product/addProduct.jsp");
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		try {
			Product product = new Product();
			DiskFileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload servletFileUpload = new ServletFileUpload(factory);
			List<FileItem> items = servletFileUpload.parseRequest(req);
			for (FileItem item : items) {
				if (item.getFieldName().equals("name")) {
					String name = item.getString();
					product.setName(name);
				} else if (item.getFieldName().equals("price")) {
					String price = item.getString();
					product.setPrice(Integer.parseInt(price));
				} else if (item.getFieldName().equals("quantity")) {
					String quantity = item.getString();
					product.setQuantity(Integer.parseInt(quantity));
				} else if (item.getFieldName().equals("mota")) {
					String mota = item.getString();
					product.setDescribe(mota);
				} else if (item.getFieldName().equals("image")) {
					String location = "F:\\anh_product\\";
					String fileName = System.currentTimeMillis() + ".jsp";
					File file = new File(location + fileName);
					item.write(file);
					product.setImage(fileName);

				}
			}
			productService.add(product);
			
			resp.sendRedirect(req.getContextPath()+"/admin/add/product");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}
}
