package com.trungtamjava.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.trungtamjava.model.User;
import com.trungtamjava.service.UserService;
import com.trungtamjava.service.impl.UserServiceImpl;
@WebServlet(urlPatterns="/admin/cart/list")
public class CartListController extends HttpServlet {
		UserService userService= new UserServiceImpl();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		List<User> user= userService.search("");
		req.setAttribute("users", user);
		
		RequestDispatcher dispatcher= req.getRequestDispatcher("/View/admin/cart/cart.jsp");
		dispatcher.forward(req, resp);
		
	}
}
