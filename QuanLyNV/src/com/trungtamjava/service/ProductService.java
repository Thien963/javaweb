package com.trungtamjava.service;

import java.util.List;

import com.trungtamjava.model.Product;

public interface ProductService {
	public void add(Product product);

	void edit(Product product);

	void delete(int id);

	List<Product> getAll(String name);

	Product getByProductId(int id);

	List<Product> getByCategoryId(int id);

	List<Product> searchByName(String name);
}
