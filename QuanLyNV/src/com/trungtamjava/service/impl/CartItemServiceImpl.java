package com.trungtamjava.service.impl;

import java.util.List;

import com.trungtamjava.dao.CartItemDAo;
import com.trungtamjava.dao.impl.CartItemDaoImpl;
import com.trungtamjava.model.CartItem;
import com.trungtamjava.service.CartItemService;

public class CartItemServiceImpl implements CartItemService {
	CartItemDAo cartItemDao= new CartItemDaoImpl();
	@Override
	public void insert(CartItem cartItem) {
		cartItemDao.input(cartItem);
		
	}

	@Override
	public void edit(CartItem cartItem) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int id) {
		cartItemDao.delete(id);
		
	}

	@Override
	public CartItem get(int id) {
		// TODO Auto-generated method stub
		return cartItemDao.get(id);
	}

	@Override
	public List<CartItem> getAll() {
		// TODO Auto-generated method stub
		return cartItemDao.list();
	}

	@Override
	public List<CartItem> search(String keyword) {
		// TODO Auto-generated method stub
		return null;
	}

}
