package com.trungtamjava.service.impl;

import java.util.List;

import com.trungtamjava.dao.CategoryDao;
import com.trungtamjava.dao.impl.CategoryDaoImpl;
import com.trungtamjava.model.Category;
import com.trungtamjava.service.CategoryService;

public class CategoryServiceImpl implements CategoryService {

	@Override
	public void add(Category c) {
		CategoryDao categoryDao= new CategoryDaoImpl();
		categoryDao.add(c);
		
	}

	@Override
	public void edit(Category p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int id) {
		CategoryDao categoryDao= new CategoryDaoImpl();
		categoryDao.delete(id);
		
	}

	@Override
	public List<Category> getAll() {
		CategoryDao categoryDao= new CategoryDaoImpl();
		return categoryDao.getAll();
	}

}
