package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.trungtamjava.model.User;

import utils.CommonConstant;
@WebFilter(urlPatterns="/admin/*")
public class AdminFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		HttpSession httpSession = req.getSession();
		Object userObj = httpSession.getAttribute("user");
		if (userObj != null) {
			User user = (User) userObj;
			if (CommonConstant.ROLE_ADMIN.equals(user.getRole())) {
				chain.doFilter(req, resp);
			}else {
				RequestDispatcher dispatcher= req.getRequestDispatcher("/View/admin/acces-deny.jsp");
			}
		}else {
			resp.sendRedirect(req.getContextPath()+"/dang-nhap");
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

}
