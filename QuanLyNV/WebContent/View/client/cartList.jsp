<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<c:url value='/Static1/images/favicon.png'/>">
     <link href="<c:url value='/Static1/css/bootstrap.css'/>" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,100italic,100' rel='stylesheet' type='text/css'>
    <link href="<c:url value='/Static1/css/font-awesome.min.css'/>" rel="stylesheet">
    <link rel="stylesheet" href="<c:url value='/Static1/css/flexslider.css'/>" type="text/css" media="screen"/>
    <link href="<c:url value='/Static1/css/style.css'/>" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js">
</script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js">
</script>
<![endif]-->
</head>
<body>
 <div class="wrapper">
      <div class="header">
        <div class="container">
          <div class="row">
            <div class="col-md-2 col-sm-2">
              <div class="logo">
                <a href="index.html">
                  <img src="images/logo.png" alt="FlatShop">
                </a>
              </div>
            </div>
            <div class="col-md-10 col-sm-10">
              <div class="header_top">
                <div class="row">
                  <div class="col-md-3">
                    <ul class="option_nav">
                      <li class="dorpdown">
                        <a href="#">
                          Eng
                        </a>
                        <ul class="subnav">
                          <li>
                            <a href="#">
                              Eng
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              Vns
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              Fer
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              Gem
                            </a>
                          </li>
                        </ul>
                      </li>
                      <li class="dorpdown">
                        <a href="#">
                          USD
                        </a>
                        <ul class="subnav">
                          <li>
                            <a href="#">
                              USD
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              UKD
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              FER
                            </a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-6">
                    <ul class="topmenu">
                      <li>
                        <a href="#">
                          About Us
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          News
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          Service
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          Recruiment
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          Media
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          Support
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <ul class="usermenu">
                      <li>
                        <a href="/QuanLyBanHang/logout" class="log">
                          Logout
                        </a>
                      </li>
                      <li>
                        <a href="checkout2.html" class="reg">
                          Register
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="clearfix">
              </div>
              <div class="header_bottom">
                <ul class="option">
                  <li id="search" class="search">
                    <form action="/QuanLyBanHang/member/search/name" method="get">
                      <input class="search-submit" type="submit" value="">
                      <input class="search-input" placeholder="Enter your search term..." type="text"  name="search">
                    </form>
                  </li>
                  <li class="option-cart">
                    <a href="#" class="cart-icon">
                      cart 
                      <span class="cart_no">
                        02
                      </span>
                    </a>
                    <ul class="option-cart-item">
                      <li>
                        <div class="cart-item">
                          <div class="image">
                            <img src="<c:url value='/Static1/images/products/thum/products-01.png'/>" alt="">
                          </div>
                          <div class="item-description">
                            <p class="name">
                              Lincoln chair
                            </p>
                            <p>
                              Size: 
                              <span class="light-red">
                                One size
                              </span>
                              <br>
                              Quantity: 
                              <span class="light-red">
                                01
                              </span>
                            </p>
                          </div>
                          <div class="right">
                            <p class="price">
                              $30.00
                            </p>
                            <a href="#" class="remove">
                              <img src="<c:url value='/Static1/images/remove.png'/>" alt="remove">
                            </a>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="cart-item">
                          <div class="image">
                            <img src="<c:url value='/Static1/images/products/thum/products-02.png'/>" alt="">
                          </div>
                          <div class="item-description">
                            <p class="name">
                              Lincoln chair
                            </p>
                            <p>
                              Size: 
                              <span class="light-red">
                                One size
                              </span>
                              <br>
                              Quantity: 
                              <span class="light-red">
                                01
                              </span>
                            </p>
                          </div>
                          <div class="right">
                            <p class="price">
                              $30.00
                            </p>
                            <a href="#" class="remove">
                              <img src="<c:url value='/Static1/images/remove.png'/>" alt="remove">
                            </a>
                          </div>
                        </div>
                      </li>
                      <li>
                        <span class="total">
                          Total 
                          <strong>
                            $60.00
                          </strong>
                        </span>
                        <button class="checkout" onClick="location.href='checkout.html'">
                          CheckOut
                        </button>
                      </li>
                    </ul>
                  </li>
                </ul>
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">
                      Toggle navigation
                    </span>
                    <span class="icon-bar">
                    </span>
                    <span class="icon-bar">
                    </span>
                    <span class="icon-bar">
                    </span>
                  </button>
                </div>
                <div class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="active dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Home
                      </a>
                      <div class="dropdown-menu">
                        <ul class="mega-menu-links">
                          <li>
                            <a href="index.html">
                              home
                            </a>
                          </li>
                          <li>
                            <a href="home2.html">
                              home2
                            </a>
                          </li>
                          <li>
                            <a href="home3.html">
                              home3
                            </a>
                          </li>
                          <li>
                            <a href="productlitst.html">
                              Productlitst
                            </a>
                          </li>
                          <li>
                            <a href="productgird.html">
                              Productgird
                            </a>
                          </li>
                          <li>
                            <a href="details.html">
                              Details
                            </a>
                          </li>
                          <li>
                            <a href="cart.html">
                              Cart
                            </a>
                          </li>
                          <li>
                            <a href="checkout.html">
                              CheckOut
                            </a>
                          </li>
                          <li>
                            <a href="checkout2.html">
                              CheckOut2
                            </a>
                          </li>
                          <li>
                            <a href="contact.html">
                              contact
                            </a>
                          </li>
                        </ul>
                      </div>
                    </li>
                     <c:forEach items="${category}" var="c">
                    <li>
                     <a href="<c:url value='/member/category?categoryId=${c.id }'/>">${c.name }</a>
                      </a>
                    </li>
                    </c:forEach>
                   
                   
                    <li>
                      <a href="productgird.html">
                        gift
                      </a>
                    </li>
                    <li>
                      <a href="productgird.html">
                        kids
                      </a>
                    </li>
                    <li>
                      <a href="productgird.html">
                        blog
                      </a>
                    </li>
                    <li>
                      <a href="productgird.html">
                        jewelry
                      </a>
                    </li>
                    <li>
                      <a href="contact.html">
                        contact us
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix">
        </div>
        <div class="page-index">
          <div class="container">
            <p>
              Home - Shopping Cart
            </p>
          </div>
        </div>
      </div>
      <div class="clearfix">
      </div>
      <div class="container_fullwidth">
        <div class="container shopping-cart">
          <div class="row">
            <div class="col-md-12">
              <h3 class="title">
                Shopping Cart
              </h3>
              <div class="clearfix">
              </div>
              <table class="shop-table">
                <thead>
                  <tr>
                    <th>
                      Image
                    </th>
                    <th>
                      Dtails
                    </th>
                    <th>
                      Price
                    </th>
                    <th>
                      Quantity
                    </th>
                    <th>
                      Price
                    </th>
                    <th>
                      Delete
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <img src="<c:url value='/Static1/images/products/small/products-06.png'/>" alt="">
                    </td>
                    <td>
                      <div class="shop-details">
                        <div class="productname">
                          Lincoln Corner Unit Products
                        </div>
                        <p>
                          <img alt="" src="<c:url value='/Static1/images/star.png'/>">
                          <a class="review_num" href="#">
                            02 Review(s)
                          </a>
                        </p>
                        <div class="color-choser">
                          <span class="text">
                            Product Color : 
                          </span>
                          <ul>
                            <li>
                              <a class="black-bg " href="#">
                                black
                              </a>
                            </li>
                            <li>
                              <a class="red-bg" href="#">
                                light red
                              </a>
                            </li>
                          </ul>
                        </div>
                        <p>
                          Product Code : 
                          <strong class="pcode">
                            Dress 120
                          </strong>
                        </p>
                      </div>
                    </td>
                    <td>
                      <h5>
                        $200.00
                      </h5>
                    </td>
                    <td>
                      <select name="">
                        <option selected value="1">
                          1
                        </option>
                        <option value="1">
                          2
                        </option>
                        <option value="1">
                          3
                        </option>
                      </select>
                    </td>
                    <td>
                      <h5>
                        <strong class="red">
                          $200.00
                        </strong>
                      </h5>
                    </td>
                    <td>
                      <a href="#">
                        <img src="<c:url value='/Static1/images/remove.png'/>" alt="">
                      </a>
                    </td>
                  </tr>
                  <c:forEach items="${sessionScope.cart}" var="itemMap" >
                  <tr>
                  	
                  	<td> <c:url value='/image?fileName=${itemMap.value.product.image }' var="imgUrl"/><img alt="" src="${imgUrl}"> </td>
                  	<td>${itemMap.value.product.name}</td>
                  	<td>${itemMap.value.sellPrice}</td>
         			<td>${itemMap.value.buyQuantity}</td>
         			<td>${itemMap.value.buyQuantity * itemMap.value.sellPrice }</td>
                  	<td><a href="<c:url value='/member/cart/delete?productId=${itemMap.key }'/>">
                        <img src="<c:url value='/Static1/images/remove.png'/>" alt="">
                      </a></td>
                  </tr>  
					</c:forEach>
                </tbody>
                <tfoot>
                  
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="6">
                    <a href="/QuanLyBanHang/member/list/product">
                      <button class="pull-left">
                        Continue Shopping
                      </button>
                      </a>
                      <form action="/QuanLyBanHang/member/oder/cart" method="post">
                      <button class=" pull-right" type="submit">
                        đặt mua
                      </button>
                      </form>
                    </td>
                  </tr>
                </tfoot>
              </table>
              <div class="clearfix">
              </div>
              <div class="row">
                
                
                <div class="col-md-4 col-sm-6" > 
                  <div class="shippingbox" align="right">
                    <div class="subtotal">
                      <h5>
                        Sub Total
                      </h5>
                      <span>
                      
                      </span>
                    </div>
                    <div class="grandtotal">
                      <h5>
                        GRAND TOTAL 
                      </h5>
                      <span>
                        $1.000.00
                      </span>
                    </div>
                    
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          <div class="clearfix">
          </div>
          <div class="our-brand">
            <h3 class="title">
              <strong>
                Our 
              </strong>
              Brands
            </h3>
            <div class="control">
              <a id="prev_brand" class="prev" href="#">
                &lt;
              </a>
              <a id="next_brand" class="next" href="#">
                &gt;
              </a>
            </div>
            <ul id="braldLogo">
              <li>
                <ul class="brand_item">
                  <li>
                    <a href="#">
                      <div class="brand-logo">
                        <img src="<c:url value='/Static1/images/envato.png'/>" alt="">
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="brand-logo">
                        <img src="<c:url value='/Static1/images/themeforest.png'/>" alt="">
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="brand-logo">
                        <img src="<c:url value='/Static1/images/photodune.png'/>" alt="">
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="brand-logo">
                        <img src="<c:url value='/Static1/images/activeden.png'/>" alt="">
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="brand-logo">
                        <img src="<c:url value='/Static1/images/envato.png'/>" alt="">
                      </div>
                    </a>
                  </li>
                </ul>
              </li>
              <li>
                <ul class="brand_item">
                  <li>
                    <a href="#">
                      <div class="brand-logo">
                        <img src="<c:url value='/Static1/images/envato.png'/>" alt="">
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="brand-logo">
                        <img src="<c:url value='/Static1/images/themeforest.png'/>" alt="">
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="brand-logo">
                        <img src="<c:url value='/Static1/images/photodune.png'/>" alt="">
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="brand-logo">
                        <img src="<c:url value='/Static1/images/activeden.png'/>" alt="">
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="brand-logo">
                        <img src="<c:url value='/Static1/images/envato.png'/>" alt="">
                      </div>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="clearfix">
      </div>
      <div class="footer">
        <div class="footer-info">
          <div class="container">
            <div class="row">
              <div class="col-md-3">
                <div class="footer-logo">
                  <a href="#">
                    <img src="<c:url value='/Static1/images/logo.png'/>" alt="">
                  </a>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <h4 class="title">
                  Contact 
                  <strong>
                    Info
                  </strong>
                </h4>
                <p>
                  No. 08, Nguyen Trai, Hanoi , Vietnam
                </p>
                <p>
                  Call Us : (084) 1900 1008
                </p>
                <p>
                  Email : michael@leebros.us
                </p>
              </div>
              <div class="col-md-3 col-sm-6">
                <h4 class="title">
                  Customer
                  <strong>
                    Support
                  </strong>
                </h4>
                <ul class="support">
                  <li>
                    <a href="#">
                      FAQ
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      Payment Option
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      Booking Tips
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      Infomation
                    </a>
                  </li>
                </ul>
              </div>
              <div class="col-md-3">
                <h4 class="title">
                  Get Our 
                  <strong>
                    Newsletter 
                  </strong>
                </h4>
                <p>
                  Lorem ipsum dolor ipsum dolor.
                </p>
                <form class="newsletter">
                  <input type="text" name="" placeholder="Type your email....">
                  <input type="submit" value="SignUp" class="button">
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="copyright-info">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <p>
                  Copyright © 2012. Designed by 
                  <a href="#">
                    Michael Lee
                  </a>
                  . All rights reseved
                </p>
              </div>
              <div class="col-md-6">
                <ul class="social-icon">
                  <li>
                    <a href="#" class="linkedin">
                    </a>
                  </li>
                  <li>
                    <a href="#" class="google-plus">
                    </a>
                  </li>
                  <li>
                    <a href="#" class="twitter">
                    </a>
                  </li>
                  <li>
                    <a href="#" class="facebook">
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript==================================================-->
    <script type="text/javascript" src="<c:url value='/Static1/js/jquery-1.10.2.min.js'/>">
    </script>
    <script type="text/javascript" src="<c:url value='/Static1/js/bootstrap.min.js'/>">
    </script>
    <script defer src="<c:url value='/Static1/js/jquery.flexslider.js'/>">
    </script>
    <script type="text/javascript" src="<c:url value='/Static1/js/jquery.carouFredSel-6.2.1-packed.js'/>">
    </script>
    <script type="text/javascript" src="<c:url value='/Static1/js/script.min.js'/>" >
    </script>

</body>
</html>